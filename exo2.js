let nbre1 = 0;//initialisation des deux premiers nombres
let nbre2 = 1;
//boucle qui affiche les 100 premiers nombres de la suite de Fibonacci
for (let i = 0; i <= 100; i++) {
    let res = nbre1 + nbre2; 
    nbre1 = nbre2;        //additionne à chaque résultat le nombre suivant
    nbre2 = res;          
    console.log(res);
}