<?php

$mot = readline("Tape un mot et je te dirai si c'est un palindrome :");
    $indexFin = strlen($mot)-1; // index fin
    $indexIni = 0; // Index départ
    $continue = true;
    $milieu = ($indexFin%2) == 0 ? ($indexFin/2) : ($indexFin/2)+1; // je défini le milieu
    // modulo pour trouver le milieu même impair
    while($continue == true){
        if($mot[$indexIni] != $mot[$indexFin]){ // si deux lettres sont différentes pas la peine de continuer !palindrome
            $continue = false;
            echo("ce n'est pas un palindrome.");
        } else if($indexIni == $milieu){
            $continue = false;
            echo("c'est un palindrome.");
        }
        $indexIni++;
        $indexFin--;
    }

?>