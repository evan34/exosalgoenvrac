<?php
$nbre1 = 0; //initialisation des deux premiers nombres
$nbre2 = 1;
//boucle qui affiche les 100 premiers nombres de la suite de Fibonacci
for ($i = 0; $i <= 100; $i++) {
    $res = $nbre1 + $nbre2;
    $nbre1 = $nbre2;        //additionne à chaque résultat le nombre suivant
    $nbre2 = $res;
    echo $nbre2. '<br />';
}

?>