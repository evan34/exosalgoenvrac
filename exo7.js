let age = 19;
let sexe = 'homme';
//conditions qui déterminent si imposable suivant l'age et le sexe.
if (age > 18 && age <= 35 && sexe == 'femme') {
    console.log('Femme doit payer');
} else if (age > 18 && sexe === 'homme') {
    console.log('Homme doit payer');
} else {
    console.log('Tu n\'es pas imposable');
}
