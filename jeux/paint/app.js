$().ready(function () {
    //creation des div pour chaque couleur
    let $whiteColor = $('<div>').attr({id: 'white', class: 'color'}).appendTo('body');
    $whiteColor.append('white');
    $whiteColor.css({backgroundColor: 'white'});

    let $blueColor = $('<div>').attr({id: 'blue', class: 'color'}).appendTo('body');
    $blueColor.append('blue');
    $blueColor.css({backgroundColor: 'blue'});

    let $yellowColor = $('<div>').attr({id: 'yellow', class: 'color'}).appendTo('body');
    $yellowColor.append('yellow');
    $yellowColor.css({backgroundColor: 'yellow'});

    let $redColor = $('<div>').attr({id: 'red', class: 'color'}).appendTo('body');
    $redColor.append('red');
    $redColor.css({backgroundColor: 'red'});

    //creation du container qui contiendra les pixels
    let $container = $('<div>').attr('id', 'container').appendTo('body');
    $container.css({display: 'flex',flexWrap: 'wrap', width: '1000px', height: '800px'}); 
    
        for (let i = 0; i <= 6000; i++) { 
        let $cube = $('<div>').attr('class', 'cube');
        $cube.css({width: '10px', height: '10px', backgroundColor: 'grey', margin: '1px'});
        $('#container').append($cube);
        }

    //creation des fonctions pour sélectionner les couleurs    
    $('#white').on('click', function() {
        $('.cube').on('click', function() {
            $(this).css('background-color', 'white');
        });
            });

    $('#blue').on('click', function() {
        $('.cube').on('click', function() {
            $(this).css('background-color', 'blue');
        });
            });        

    $('#yellow').on('click', function() {
        $('.cube').on('click', function() {
            $(this).css('background-color', 'yellow');
        });
            });       
    
    $('#red').on('click', function() {
        $('.cube').on('click', function() {
            $(this).css('background-color', 'red');
        });
            }); 
            
    $('.cube').on('dblclick', function() {
        $(this).css('background-color', 'grey');
    });        
});



