$(document).ready(function () {

    let player1 = prompt('Joueur 1 entrez votre prénom');
    let player2 = prompt('Joueur 2 entrez votre prénom');


    let tousLesDiv = document.getElementsByTagName('div');
    for (let i = 0; i < tousLesDiv.length; i++) {
        tousLesDiv.item(i).onclick = function () {
            console.log(this.id);
        }
    }
    /*Essai de définir le tour du joueur
      let joueur1;
      let joueur2;*/

    let arrow = function () {           
        //joueur1 = $('#player1').text('A ' + player1 + ' de jouer');
        $(".divMorpion").on('click', function () {
            $(this).removeAttr('class', 'divMorpion').attr('class', 'arrowTarget');
            verif();                
            circle();             
        });
    }

    let circle = function () {
        //joueur2 = $('#player2').text('A ' + player2 + ' de jouer');
        $(".divMorpion").on('click', function () {
            $(this).removeAttr('class', 'divMorpion').attr('class', 'arrowCircle');
            verif();           
            arrow();               
        });            
    };
          
    arrow();

    let verif = function () {

        if($('#case1').attr("class") === 'arrowTarget' && $('#case2').attr("class") === 'arrowTarget' && $('#case3').attr("class") === 'arrowTarget'){
            gagne();
        }
        if($('#case1').attr("class") === 'arrowCircle' && $('#case2').attr("class") === 'arrowCircle' && $('#case3').attr("class") === 'arrowCircle'){
            gagne();
        }

        if($('#case4').attr("class") === 'arrowTarget' && $('#case5').attr("class") === 'arrowTarget' && $('#case6').attr("class") === 'arrowTarget'){
            gagne();
        }
        if($('#case4').attr("class") === 'arrowCircle' && $('#case5').attr("class") === 'arrowCircle' && $('#case6').attr("class") === 'arrowCircle'){
            gagne();
        }

        if($('#case7').attr("class") === 'arrowTarget' && $('#case8').attr("class") === 'arrowTarget' && $('#case9').attr("class") === 'arrowTarget'){
            gagne();
        }
        if($('#case7').attr("class") === 'arrowCircle' && $('#case8').attr("class") === 'arrowCircle' && $('#case9').attr("class") === 'arrowCircle'){
            gagne();
        }

        if($('#case1').attr("class") === 'arrowTarget' && $('#case5').attr("class") === 'arrowTarget' && $('#case9').attr("class") === 'arrowTarget'){
            gagne();
        }
        if($('#case1').attr("class") === 'arrowCircle' && $('#case5').attr("class") === 'arrowCircle' && $('#case9').attr("class") === 'arrowCircle'){
            gagne();
        }

        if($('#case3').attr("class") === 'arrowTarget' && $('#case5').attr("class") === 'arrowTarget' && $('#case7').attr("class") === 'arrowTarget'){
            gagne();
        }
        if($('#case3').attr("class") === 'arrowCircle' && $('#case5').attr("class") === 'arrowCircle' && $('#case7').attr("class") === 'arrowCircle'){
            gagne();
        }

        if($('#case1').attr("class") === 'arrowTarget' && $('#case4').attr("class") === 'arrowTarget' && $('#case7').attr("class") === 'arrowTarget'){
            gagne();
        }
        if($('#case1').attr("class") === 'arrowCircle' && $('#case4').attr("class") === 'arrowCircle' && $('#case7').attr("class") === 'arrowCircle'){
            gagne();
        }

        if($('#case2').attr("class") === 'arrowTarget' && $('#case5').attr("class") === 'arrowTarget' && $('#case8').attr("class") === 'arrowTarget'){
            gagne();
        }
        if($('#case2').attr("class") === 'arrowCircle' && $('#case5').attr("class") === 'arrowCircle' && $('#case8').attr("class") === 'arrowCircle'){
            gagne();
        }

        if($('#case3').attr("class") === 'arrowTarget' && $('#case6').attr("class") === 'arrowTarget' && $('#case9').attr("class") === 'arrowTarget'){
            gagne();
        }
        if($('#case3').attr("class") === 'arrowCircle' && $('#case6').attr("class") === 'arrowCircle' && $('#case9').attr("class") === 'arrowCircle'){
            gagne();
        }
        
    }
     
    let gagne = function() {
        $("#win").modal('show');
        $('#replay').on('click', function() {
            location.reload();
        });
    };
    
});