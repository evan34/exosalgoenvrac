$().ready(function () {
    let tours = ['', '', '', '', '', '', '', '', ''];
    let ia = '';
    let tour = '';
    let compteur = 0;
    let jeu = false;

    let joueur = prompt('Choisissez: X ou O').toUpperCase();

    switch (joueur) {
        case 'X':
        ia = 'O';
        tour = 'X';
        $('#player').html('à ' + tour + ' de jouer');
        break;
        
        case 'O':
        ia = 'X';
        tour = 'O';
        $('#player').html('à ' + tour + ' de jouer');
        break;

        case null:
        $("#recommencer").modal('show');
        $('#restart').on('click', function() {
            window.location.reload(true);
        });
        
        default:
        $("#recommencer").modal('show');
        $('#restart').on('click', function() {
            window.location.reload(true);
        });  
    }

    function tourJoueur (tour, id) {
        let caseJouee = $('.divMorpion' + id).text();
        if (caseJouee === '') {
            compteur++;
            tours[id] = tour;
            $(''+id).text(tour);
            gagne(tours, tour);
            if (jeu === false) {
                tourIa();
                $('#player').html('à ' + tour + ' de jouer');
                gagne(tours, tourIa);
            }
        }
    }

    function tourIa () {
        let casePrise = false;
        while (casePrise === false && compteur !== 5) {
            let joueIa = (Math.random() * 10).toFixed();
            let joue = $('' + joueIa).text();
            if (joue === '') {
                $('' + joueIa).text(ia);
                casePrise = true;
                tours[joueIa] = ia;
            }
        }
    }

    

});