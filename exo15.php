<?php
/*On a un tableau contenantdesvaleurs [26,5,14,-8,1,12]
Au début on analyse si le premier élément est supérieur au second 
(5 et1) si c’est vrai on permute les deux éléments (swap), 
le 1 prend la place du 5. On continue. 
On regarde si le deuxième élément est supérieur au troisième 
(ici 5 et 12). Comme 5 n’est pas supérieur à 12 on ne fait rien. 
On continue à regarder si on doit permuter les deux éléments contigus 
jusqu’à la fin du tableau.
A la fin de cette première boucle le tableau n’est pas complétement trié. 
Il faut recommencer à regarder si le premier élément est supérieur au deuxième 
puis si  le  deuxième  est  supérieur  au  troisième etc... 
A chaque fois on fait des permutations si l’élément N est supérieur
à l’élément N+1. On va voir des bulles remonter d’où le nom du tri.
Quand sait-on que le tableau est trié et que l’on doit arrêter le tri?
Quand on ne fait plus aucune permutation dans le tableau 
(utiliser un booléen qui indique s’il y a eu des permutations).*/



$tabsort = array(26,5,14,-8,1,12);//tableau de référence
$valTemp = 0;                     //valeurs temporaires à réafecter au tableau
$orderedArray = false;            //tableau non ordonné
$taille =sizeof($tabsort);        //taille du tableau


while($orderedArray == false) {
      $orderedArray = true;
   for ($i= 0; $i < $taille-1; $i++) {  
       if ($tabsort[$i] > $tabsort[$i+1]){

           $valTemp = $tabsort[$i+1];    //0 = 5
           $tabsort[$i+1] = $tabsort[$i];//5 = 26
           $tabsort[$i] = $valTemp;      //26 = 0
           $orderedArray = false;
       }
   }//$taille--;
}

foreach ($tabsort as $value) {
   echo($value."<br>");
}
?>