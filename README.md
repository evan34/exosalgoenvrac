### Ajax
* Formulaire simple (récupération de données)
* Gogol (moteur de recherche fichier Json)
* Jour de naissance (renvoie le jour de la semaine)

### Jeux
* Morpion (à terminer pour l'IA)
* Paint (Pixel'Art)
* Pendu (à revoir)



### EXOS ALGORITHMES

### TodoList
* revoir les exos 
  * php
   * 9, 11, 13

### JAVASCRIPT       
 * exo 1: Compteur 10 à 100.                       
 * exo 2: La suite de Fibonacci.
 * exo 3: Chaine inversée.
 * exo 4: Triangle de Pascal.
 * exo 5: Triangle de Pascal inversé.
 * exo 6: Prix de n photocopies à n tarif.
 * exo 7: Imposable ? non-imposable.
 * exo 8: Tarifs d'assurance suivant plusieurs critères.
 * exo 9: Code de César
 


### PHP
  * exo 1: Compteur 10 à 100. 
  * exo 2: La suite de Fibonacci.
  * exo 3: Chaine inversée.
  * exo 4: Triangle de Pascal.
  * exo 5: Triangle de Pascal inversé.
  * exo 6: Prix de n photocopies à n tarif.
  * exo 7: Imposable ? non-imposable.
  * exo 8: Tarifs d'assurance suivant plusieurs critères.
  * exo 9: Code de César
  * exo 10: Connection utilisateur
  * exo 11: Palindrome
  * exo 12: Creation de table et affichage des valeurs
  * exo 13: Note superieure
  * exo 14: Nombre le plus petit
  * exo 15: Tri de valeurs
  * exo 16: Tri par Dichotomie

  
2019
