function ajaxing() {
    xhr = getxhr();
    //changement des états de façon asynchrone de 0 à 4
    xhr.onreadystatechange=function() {
        if (xhr.readyState === 4)//réponse reçue
            document.getElementById("reponse").innerHTML = xhr.responseText;
        else
            document.getElementById("reponse").innerHTML = "Hey Man !";
    }
    //appel de la page traitement.php: méthode, url, asynchrone
    xhr.open('POST', 'traitement.php', true);
    //définit un en-tête de requête http: nom(type du corps de la requete), valeur(encodé sous forme clé/valeur)
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //envoi de la requete
    xhr.send("dNaissance="+document.getElementById("dNaissance").value);
}
  