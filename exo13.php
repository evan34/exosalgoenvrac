<?php
/*Saisir  5  notes  et  afficher  les  notes  qui  sont  supérieures  à  10.  
Réaliser  cet exercice avec deux boucles.
La première boucle servira à construire le tableau avec les saisies, 
la seconde permettra d'afficher les notes supérieures à 10.
Exemple si l'utilisateur a saisi 5 14 13 6 3. 
Affichage final sur l'écran :Note supérieure à 10 : 14Note supérieure à 10 : 1*/


$note1 = 5;
$note2 = 14;
$note3 = 13;    
$note4 = 6;
$note5 = 3;
$nbr = 5;
$arrayNote =[];
for ($i=1; $i < $nbr; $i++):
    $arrayNote[] = ${'note'.$i};
endfor;
foreach ($arrayNote as $noteSup):
    if ($noteSup >= 10):
    echo '<p>Note supérieure à 10 : '.$noteSup.'</p>';
    endif;
endforeach;

?> 




