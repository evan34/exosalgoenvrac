<?php
$age = 30;
$datePermis = 6;
$nbreAccident = 2;
$nbreAnneesAssurance = 10;
$tarifA = 'Tarif A';
$tarifB = 'Tarif B';
$tarifC = 'Tarif C';
$tarifD = 'Tarif D';
$refus = 'REFUS';
//condition du tarif A pour l'age, la date du permis, le nombre d'accident
if ($age < 25 && $datePermis < 2 && $nbreAccident === 0) {
    echo($tarifA);
    //conditions du tarif B ou D ou REFUS pour l'age, la date du permis, le nombre d'accident
} else if (($age < 25 && $datePermis > 2) || ($age > 25 && $datePermis < 2)) {
    if ($nbreAccident === 0) {
        echo($tarifB);
    } else if ($nbreAccident === 1) {
        echo($tarifD);
    } else if ($nbreAccident > 1) {
        echo($refus);
    }
    //conditions du tarif dégréssif A, B, C, D pour le nombre d'années d'assurance
} else if ($age > 25 && $datePermis > 2 && $nbreAccident === 0) {
    echo($tarifB);
    } else if ($age > 25 && $datePermis > 2 && $nbreAccident === 2) {
        echo($tarifD);
    } else if ($age > 25 && $datePermis > 2 && $nbreAccident > 2){
        echo($refus);
    } else if ($nbreAnneesAssurance > 1 && $tarifA) {
        echo($tarifB);
    } else if ($nbreAnneesAssurance > 1 && $tarifB) {
        echo($tarifC);
    } else if ($nbreAnneesAssurance > 1 && $tarifC) {
        echo($tarifD);
    } else {
        echo($tarifA);
    }
    ?>